package io.gamesys.register.controller;

import static io.gamesys.register.constants.Constants.DATE_FORMAT;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.gamesys.register.exception.DuplicateRegisterationException;
import io.gamesys.register.exception.ExcludedUserException;
import io.gamesys.register.model.User;

@RunWith(SpringRunner.class)
@WebMvcTest
public class ValidationControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	UserRegisterService userService;

	@MockBean
	ExclusionServiceImpl exclusionService;

	@InjectMocks
	private ValidationController validationController;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(validationController).build();
	}

	@Test
	public void testRegisterUserSuccess() throws Exception {
		User user = new User("testUser2", "Password1", getDateFromString("1815-12-11"), "123-45-6989");
		when(exclusionService.validate(any(), any())).thenReturn(true);
		when(userService.findBySsn(any())).thenReturn(null);
		this.mockMvc.perform(post("/user/register").contentType(MediaType.APPLICATION_JSON).content(asJsonString(user)))
				.andExpect(status().isOk());
	}

	@Test(expected = NestedServletException.class)
	public void testAlreadyRegisteredUser() throws Exception {
		User user = new User("testUser2", "Password1", getDateFromString("1815-12-11"), "123-45-6789");
		when(exclusionService.validate(any(), any())).thenReturn(true);
		when(userService.findBySsn(any())).thenReturn(user);
		this.mockMvc.perform(post("/user/register").contentType(MediaType.APPLICATION_JSON).content(asJsonString(user)))
				.andExpect(request().asyncStarted()).andReturn();

	}

	@Test(expected = NestedServletException.class)
	public void testUserInExceptionList() throws Exception {
		User user = new User("adaLovelace", "Analytical3ngineRulz", getDateFromString("1815-12-10"), "85385075");
		when(exclusionService.validate(any(), any())).thenReturn(false);
		when(userService.findBySsn(any())).thenReturn(null);
		this.mockMvc.perform(post("/user/register").contentType(MediaType.APPLICATION_JSON).content(asJsonString(user)))
				.andExpect(request().asyncStarted()).andReturn();
	}

	@Test
	public void testInvalidPassword() throws Exception {
		User user = new User("testUser1", "password", getDateFromString("1815-12-10"), "123-45-6789");
		this.mockMvc.perform(post("/user/register").contentType(MediaType.APPLICATION_JSON).content(asJsonString(user)))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testInvalidUserName() throws Exception {
		User user = new User("tes User1", "password", getDateFromString("1815-12-10"), "123-45-6789");
		this.mockMvc.perform(post("/user/register").contentType(MediaType.APPLICATION_JSON).content(asJsonString(user)))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testInvalidSsn() throws Exception {
		User user = new User("tes User1", "password", getDateFromString("1815-12-10"), "12394596789");
		this.mockMvc.perform(post("/user/register").contentType(MediaType.APPLICATION_JSON).content(asJsonString(user)))
				.andExpect(status().isBadRequest());
	}
	@Test
	public void testInvalidDate() throws Exception {
		User user = new User("tes User1", "password", getDateFromString("10-12-1999"), "12394596789");
		this.mockMvc.perform(post("/user/register").contentType(MediaType.APPLICATION_JSON).content(asJsonString(user)))
				.andExpect(status().isBadRequest());
	}
	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static Date getDateFromString(String date) {
		Date parsed;
		try {
			SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
			parsed = format.parse(date);
		} catch (java.text.ParseException pe) {
			throw new IllegalArgumentException(pe);
		}
		return parsed;
	}
}
