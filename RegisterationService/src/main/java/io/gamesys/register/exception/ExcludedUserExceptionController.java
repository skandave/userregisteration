package io.gamesys.register.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
@ControllerAdvice
public class ExcludedUserExceptionController  {
	
	 @ExceptionHandler(value = ExcludedUserException.class)
	   public ResponseEntity<Object> exception(ExcludedUserException exception) {
	      return new ResponseEntity<>("The User is in Exclusion List", HttpStatus.PRECONDITION_FAILED);
	   }

}
