package io.gamesys.register.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DuplicateRegisterationExceptionController {
	 @ExceptionHandler(value = DuplicateRegisterationException.class)
	   public ResponseEntity<Object> exception(DuplicateRegisterationException exception) {
	      return new ResponseEntity<>("The User is already Registered", HttpStatus.PRECONDITION_FAILED);
	   }
}
