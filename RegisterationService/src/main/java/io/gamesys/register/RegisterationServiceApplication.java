package io.gamesys.register;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class RegisterationServiceApplication {
	
	private final static Logger logger = LoggerFactory.getLogger(RegisterationServiceApplication.class);
	
	public static void main(String[] args) {
		logger.info("User Registeration Service Started");
		SpringApplication.run(RegisterationServiceApplication.class, args);
	}

}