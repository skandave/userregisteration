package io.gamesys.register.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gamesys.register.model.User;
import io.gamesys.register.model.UserRepository;

@Service
public class UserRegisterService {

	@Autowired
	UserRepository userRepository;

	public User save(User user) {
		return userRepository.save(user);
	}

	public User findBySsn(String ssn) {
		List<User> userList = userRepository.findBySsn(ssn);
		
		if (userList == null || userList.isEmpty())
			return null;
		else
			return userList.get(0);
	}

}
