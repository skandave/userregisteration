package io.gamesys.register.controller;

import org.springframework.stereotype.Service;
import io.gamesys.register.model.User;

import java.text.SimpleDateFormat;
import java.util.Date;

import static io.gamesys.register.constants.Constants.DATE_FORMAT;

@Service
public class ExclusionServiceImpl implements ExclusionService {

	static User[] excludedUser = {
			new User("adaLovelace", "Analytical3ngineRulz", getDateFromString("1815-12-10"), "85385075"),
			new User("alanTuring", "eniGmA123", getDateFromString("1912-06-23"), "123456789"),
			new User("konradZuse", "zeD1", getDateFromString("1910-06-22"), "987654321") };

	@Override
	public boolean validate(String dateOfBirth, String ssn) {

		for (User user : excludedUser) {
			if (user.getSsn().equals(ssn) && user.getDob().equals(getDateFromString(dateOfBirth)))
				return false;
		}
		return true;
	}

	public static Date getDateFromString(String date) {
		Date parsed;
		try {
			SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
			parsed = format.parse(date);
		} catch (java.text.ParseException pe) {
			throw new IllegalArgumentException(pe);
		}
		return parsed;
	}

}
