package io.gamesys.register.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gamesys.register.exception.DuplicateRegisterationException;
import io.gamesys.register.exception.ExcludedUserException;
import io.gamesys.register.model.User;
import lombok.extern.slf4j.Slf4j;

import static io.gamesys.register.constants.Constants.DATE_FORMAT;

@RestController
@RequestMapping("/user")
@Slf4j
public class ValidationController {

	@Autowired
	UserRegisterService userService;

	@Autowired
	ExclusionServiceImpl exclusionService;

	static DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
	private static final Logger logger = LogManager.getLogger(ExclusionServiceImpl.class);

	@PostMapping("/register")
	public ResponseEntity<String> validateRegister(@Valid @RequestBody User user) {

		if (!exclusionService.validate(dateFormat.format(user.getDob()), user.getSsn())) {
			logger.info(String.format("The User with username %s is a part of the exclusion list", user.getUsername()));
			throw new ExcludedUserException();
		}

		if (isUserAlreadyRegistered(user.getSsn())) {
			logger.info(String.format("The User with Username %s is already registered", user.getUsername()));
			throw new DuplicateRegisterationException();
		}

		userService.save(user);
		return ResponseEntity.ok("Registeration Success");
	}

	@GetMapping("/ping")
	public String servicePing() {
		return "pong";
	}

	public boolean isUserAlreadyRegistered(String ssn) {
		return ObjectUtils.isEmpty(userService.findBySsn(ssn)) ? false : true;
	}
}
