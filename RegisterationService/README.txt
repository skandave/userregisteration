### Run the following for Successful Registeration

curl -d '{"username":"testUser2", "password":"Password1" , "dob":"1815-12-11", "ssn":"123-45-6989"}' -H "Content-Type: application/json" -X POST http://localhost:8089/user/register
curl -d '{"username":"1testUser", "password":"sd3hJkfa" , "dob":"2001-12-11", "ssn":"123456989"}' -H "Content-Type: application/json" -X POST http://localhost:8089/user/register
curl -d '{"username":"samTest1", "password":"Happy123" , "dob":"1980-09-11", "ssn":"546456466"}' -H "Content-Type: application/json" -X POST http://localhost:8089/user/register


### Run the following for Unsuccessful  Registeration - exclusion list
curl -d '{"username":"konradZuse", "password":"zeD1" , "dob":"1910-06-22", "ssn":"987654321"}' -H "Content-Type: application/json" -X POST http://localhost:8089/user/register
curl -d '{"username":"alanTuring", "password":"eniGmA123" , "dob":"1912-06-23", "ssn":"123456789"}' -H "Content-Type: application/json" -X POST http://localhost:8089/user/register
curl -d '{"username":"adaLovelace", "password":"Analytical3ngineRulz" , "dob":"1815-12-10", "ssn":"85385075"}' -H "Content-Type: application/json" -X POST http://localhost:8089/user/register


### Further Improvements

1. Have used a fixed list for exclusion users list, this can be worked upon as a separate service that can be called if the list is going to increase in size.

